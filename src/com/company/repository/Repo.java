package com.company.repository;

import com.company.domain.Tuple;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Repo {
    private static Repo repo = null;
    private String url="jdbc:postgresql://localhost:5432/SocialNetwork";
    private String username="postgres";
    private String password="postgres";

    private Repo(){}

    public static Repo getResource(){
        if(repo ==null)
            repo = new Repo();
        return repo;
    }

    /**
     *
     * @param email
     * @return
     */
    public int getNrMessages(String email){
        int result=0;
        String sqlVerify = "select COUNT(*) from message_recipient where email = ?";
        try(Connection connection = DriverManager.getConnection(this.url, this.username, this.password);
            PreparedStatement preparedStatement2 = connection.prepareStatement(sqlVerify))
        {
            preparedStatement2.setString(1,email);
            ResultSet resultSet = preparedStatement2.executeQuery();
            if(resultSet.next()){
                result= resultSet.getInt(1);
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    /**
     *
     * @param email
     * @return
     */
    public List<Integer> getUserGroups(String email){
        List<Integer> group_ids=new ArrayList<>();
        String sqlVerify = "select group_id from group_user where email = ?";
        try(Connection connection = DriverManager.getConnection(this.url, this.username, this.password);
            PreparedStatement preparedStatement2 = connection.prepareStatement(sqlVerify))
        {
            preparedStatement2.setString(1,email);
            ResultSet resultSet = preparedStatement2.executeQuery();
            while (resultSet.next()){
                group_ids.add(resultSet.getInt(1));
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return group_ids;
    }

    /**
     * checks for new msj of a group
     * @param integer the group id
     * @return the nr of msj of the group
     */
    public int getGroupMsj(Integer integer) {
        int result=0;
        String sqlVerify = "select COUNT(*) from message_group where id_group = ?";
        try(Connection connection = DriverManager.getConnection(this.url, this.username, this.password);
            PreparedStatement preparedStatement2 = connection.prepareStatement(sqlVerify))
        {
            preparedStatement2.setInt(1,integer);
            ResultSet resultSet = preparedStatement2.executeQuery();
            if(resultSet.next()){
                result= resultSet.getInt(1);
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    /**
     * gets the list of group ids of a user
     * @param id the user id's
     * @return a map of integer and the time to be notify
     */
    public Map<Integer,String> getEventsIds(String id){
        Map<Integer,String> result= new HashMap<>();
        String sqlVerify = "select * from user_event where email = ?";
        try(Connection connection = DriverManager.getConnection(this.url, this.username, this.password);
            PreparedStatement preparedStatement2 = connection.prepareStatement(sqlVerify))
        {
            preparedStatement2.setString(1,id);
            ResultSet resultSet = preparedStatement2.executeQuery();
            while (resultSet.next()){
                result.put(resultSet.getInt("id_event"),resultSet.getString("notification"));
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    public Tuple<Integer,Tuple<LocalDate, Time>> getEvent(Integer id){
        Tuple<Integer,Tuple<LocalDate,Time>> result=null;
        String sqlVerify = "select * from event where id = ?";
        try(Connection connection = DriverManager.getConnection(this.url, this.username, this.password);
            PreparedStatement preparedStatement2 = connection.prepareStatement(sqlVerify))
        {
            preparedStatement2.setInt(1,id);
            ResultSet resultSet = preparedStatement2.executeQuery();
            if(resultSet.next()){
                LocalDate date = LocalDate.parse(resultSet.getString("date"));
                result= new Tuple<>(id,new Tuple(date,resultSet.getTime("dtime")));
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    public String getNotificationTime(int id_event){
        String sqlVerify = "select notification from user_event where id_event = ?";
        String notify=null;
        try(Connection connection = DriverManager.getConnection(this.url, this.username, this.password);
            PreparedStatement preparedStatement2 = connection.prepareStatement(sqlVerify))
        {
            preparedStatement2.setInt(1,id_event);
            ResultSet resultSet = preparedStatement2.executeQuery();
            if(resultSet.next()){
                notify=resultSet.getString("notification");
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return notify;
    }

}
