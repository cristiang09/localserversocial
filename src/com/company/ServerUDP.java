package com.company;

import com.company.domain.Tuple;
import com.company.repository.Repo;

import java.io.IOException;
import java.net.*;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServerUDP extends Thread{

    private DatagramSocket socket;
    private int clientPort;
    private InetAddress address;
    private byte[] buf;
    private boolean running= true;
    private String email;
    private Repo repo;
    private int oldNrMessages=0;
    private int oldGroupMsj=0;
    private Map<Integer, Integer> groupMsjs= new HashMap<>();
    private Map<Integer,String> events= new HashMap<>();

    public ServerUDP(int clientPort, int serverPort,String email) throws IOException {
        this.clientPort= clientPort;
        socket= new DatagramSocket(serverPort);
        address= InetAddress.getByName("127.0.0.1");
        this.email= email;
        repo= Repo.getResource();

    }

    /**
     * checks for new private msj
     * returns the nr of new private msj of the user
     */
    private boolean newMsj(){
        int msj= repo.getNrMessages(email);
        if(msj>oldNrMessages)
        {
            int nrNewMsj= msj-oldNrMessages;
            oldNrMessages=msj;
            return true;
        }
        return false;
    }

    /**
     *  check for new msj in the groups in which user is a participant
     * @return the new
     */
    private boolean newGroupMsj(){
        boolean newMsj= false;
        List<Integer>group_ids= repo.getUserGroups(email);
        if(group_ids.size()>0){
            for (Integer integer : group_ids) {

                if (!groupMsjs.containsKey(integer)) {
                    int groupMsj = repo.getGroupMsj(integer);
                    groupMsjs.put(integer, groupMsj);
                } else {

                    int newGroupMsj = repo.getGroupMsj(integer);
                    int dif= newGroupMsj- groupMsjs.get(integer);
                    if (dif > 0) {
                        groupMsjs.put(integer,newGroupMsj);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean eventNotify(){
        int nrEv= events.size();
        Map<Integer,String> newEvents= repo.getEventsIds(email);
        if(newEvents.size()>nrEv){
            events=newEvents;
            events.forEach((integer, s) -> System.out.println(integer+" "+ s));
        }
        else if(newEvents.size()<nrEv)
            events=newEvents;

        events.forEach((integer, s) -> {
            String notify= repo.getNotificationTime(integer);
            events.put(integer,notify);
        });


        for (Integer integer : events.keySet()) {
            if(events.get(integer)!=null) {
                Tuple<Integer, Tuple<LocalDate, Time>> event = repo.getEvent(integer);
                if (event.getRight().getLeft().equals(LocalDate.now())) {
                    Integer minNotify = Integer.valueOf(events.get(integer));
                    Time eventTime = event.getRight().getRight();
                    LocalTime timen= LocalTime.now();
                    Time now = new Time(timen.getHour(),timen.getMinute()+minNotify,0);

                    if (eventTime.equals(now)) {
                        events.remove(integer);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void run() {
        boolean run = true;
        while(run){

            try {
                Thread.sleep(500);
                //default code
                String code="1";
                if(newMsj()){
                    code= "2";
                }
                else if(newGroupMsj()){
                    code= "3";
                }
                else if(eventNotify()){
                    code ="4";
                }
                buf=code.getBytes();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(!running) {
                run = false;
                buf="0".getBytes();
            }

            DatagramPacket packet= new DatagramPacket(buf,buf.length,address,clientPort);
            try {
                socket.send(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setStatus(boolean status) {
        this.running = status;
    }

    public void setClientPort(int newClientPort){
        this.clientPort= newClientPort;
    }
}
