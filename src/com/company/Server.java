package com.company;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Map;

public class Server {
    private ServerSocket serverSocket;
    public Map<String, ServerUDP> threaduri;
    public int udpStartPort= 1233;
    public void start() throws IOException {
        threaduri= new HashMap<>();
        serverSocket= new ServerSocket(5555);
        while (true){
            new ServerTCP(serverSocket.accept(),this).start();
        }
    }

    public void stop() throws IOException {
        serverSocket.close();
    }
    public int getUdpPort(){
        udpStartPort++;
        return udpStartPort;
    }
}
