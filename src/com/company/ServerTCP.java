package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerTCP extends Thread{
    private Socket clientSocket;
    private BufferedReader in;
    private PrintWriter out;
    private Server parent;

    public ServerTCP(Socket clientSocket,Server server){
        this.clientSocket= clientSocket;
        parent= server;
    }

    @Override
    public void run() {
        try {
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                String email=in.readLine();

                System.out.println(email);

                if(email.contains(";")&&email.split(";")[1].equals("close")){
                    System.out.println(email);
                    String email1= email.split(";")[0];
                    System.out.println(email1+"disconect");
                    parent.threaduri.get(email1).setStatus(false);
                    parent.threaduri.remove(email1);
                    email = null;
                    out.println("0");
                }
                if(parent.threaduri.containsKey(email)){
                    System.out.println(email);
                    //send the port back to create the udp client
                    out.println(clientSocket.getPort());
                    parent.threaduri.get(email).setClientPort(clientSocket.getPort());
                    System.out.println(email+"reconect");
                    email=null;
                }

                if(email!=null) {
                    //send the port back to create the udp client
                    out.println(clientSocket.getPort());
                    ServerUDP udpServerUDP = new ServerUDP(clientSocket.getPort(),parent.getUdpPort(),email);
                    udpServerUDP.start();
                    parent.threaduri.put(email,udpServerUDP);
                    System.out.println(email + " " + clientSocket.getPort()+ "connect");
                }
                in.close();
                out.close();
                clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
